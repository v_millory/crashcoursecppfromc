#include "functions.h"


void increment_copy(int a){
    a++; //(a=a+1)
    //here we act on a variable a in the stack
    //this variable is destoyed once the function is over
}

void increment_ptr(int *a){
    //here: work on the adress of variable a
    *a = *a+1; 
    //*a: allow to work on the varable a at the
    //adress stored in the param
}

void init_animal_copy(Animal a, char* p_name, int p_age){
    a.age = p_age;
    // a.name = p_name; forbiden because this are the adresses of the array
    for(int i=0; i<20; i++)
        a.name[i] = p_name[i];
}

void init_animal_ptr(Animal *a, char* p_name, int p_age){
    a->age = p_age; //a->age is a shortcut for (*a).age
    // a.name = p_name; forbbiden because this are the adresses of the array
    for(int i=0; i<20; i++)
        a->name[i] = p_name[i];
}

//not a problem here to copy: because we don't change animal
void display_animal(Animal a){
    printf("This animal is named: %s and is %d years old \n", a.name, a.age);
}
//rq: copy structures is usually bad memory speaking: takes time and memory

void increment_animal_age_copy(Animal a){
    a.age=a.age+1;
}

void increment_animal_age_prt(Animal *a){
    a->age= a->age +1;
}
