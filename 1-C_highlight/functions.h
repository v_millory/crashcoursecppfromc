#include <stdio.h>  //printf, scanf
#include <stdlib.h> //pointer etc

typedef struct s_animal Animal;// to avoid typing "struct s_animal" each time
struct s_animal{ //the structure itself
    int age;
    char name[20];
};

void increment_copy(int a);
void increment_ptr(int *a);

void init_animal_copy(Animal a, char* p_name, int p_age);
void init_animal_ptr(Animal *a, char* p_name, int p_age);
void display_animal(Animal a);
void increment_animal_age(Animal a);
