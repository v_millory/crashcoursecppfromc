#include <stdio.h>  //printf, scanf
#include <stdlib.h> //pointer etc

#include "functions.h"

int main( int argc, char* argv[]){
    
    //display/get value
    printf("This is a test program\n");
    char c='a';
    scanf("%c", &c);
    printf("c= %c\n", c);
    
    /* Console results:
        This is a test program
        c
        c= c
    */
    
    //reminder about the stack and heap/copying parameters:
    int n=42;
    printf("Initial number: n=%d \n", n);
    increment_copy(n);
    printf("After calling increment copy: n=%d \n", n);
    increment_ptr(&n);//gives the adress of n
    printf("After calling increment copy: n=%d \n", n);
    
    /* Console results:
        This is a test program
        Initial number: n=42 
        After calling increment copy: n=42 
        After calling increment copy: n=43 
        
    explaination: with increment_copy: a in the function is a copy
    */
    
    //play with structures
    Animal my_animal;//reserve a space memory (in the stack of the function main)
    printf("My animal just after creation: \t");
    display_animal(my_animal);
    
    printf("My animal just after init copy: \t");
    init_animal_copy(my_animal, "Toto", 5);
    display_animal(my_animal);
    
    printf("My animal just after init ptr: \t");
    init_animal_ptr(&my_animal, "Toto", 5);
    display_animal(my_animal);
    
    /* Console results:
        My animal just after creation: 	This animal is named: �U and is 1127891904 years old 
        My animal just after init copy: 	This animal is named: �U and is 1127891904 years old 
        My animal just after init ptr: 	This animal is named: Toto and is 5 years old 
    
    explaination:  -the garbadge of line 1: because try to interpret what was left in
                    the memory before that memory space was reserved for animal
                   -the garbadge of line 2: the function worked on a copy of the object
                    so doesn't change anything
                   -final line: it worked because we worked on the right memory
                    space using pointer
                    
    */
    
    //bonus reminder: dynamic allocation: to use the heap
    //what is allocated this way won't be lost after the function is over
    
    // int *number = calloc(1, sizeof(*number)); //with int
    // free(number); //to release memory
    // for structures:
    // Animal *my_dynamic_animal = calloc(1, sizeof *my_dynamic_animal); //wih struct
    // init_animal_ptr(my_dynamic_animal, "Titi", 42);
    // display_animal(*my_dynamic_animal);
}
