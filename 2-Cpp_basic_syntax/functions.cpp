//same role as in C, but we can also describe class

#include "functions.hpp"

using namespace std;

void increment_copy(int a){
    a++;
}

void increment_ptr(int *a){
    *a = *a+1; 
}

void increment_ref(int& a){
    a++;
}

// void increment_ref(int const &a){
//     a++;//this will cause the compiler to insult you
// }
//very usefull when it comes to large structure/object:
//you don't want to copy it, but you don't want that it change
//anything on the struct/object
