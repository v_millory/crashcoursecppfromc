//use .hpp extension, .h works as well but better to avoid confusion

#include <iostream>
#include <string>

//use "using namespace X" in a header can cause pb...

void increment_copy(int a);
void increment_ptr(int *a);
void increment_ref(int &a); //or int& 
//void increment_ref_c(int const &a);//won't be able to change a
