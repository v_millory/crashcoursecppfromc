#include <iostream> //to print and scan
#include <string>

#include "functions.hpp"

using namespace std;
//useful to avoid typing std:: in front of cout
//which means the function/object/def in the std library

int main(int argc, char* argv[]){ //or just main()
    
    //cout is a stream (special object) to which we can send stuff
    // << is the operator which to send
    // endl means end of the line, avoid to use "\n"
    cout << "Hello world in C++"<< endl ;
    //rq: if not using namespace std on the top:
    //  need to use std::cout and std::endl
    // << called inservtion operator
    // >> called extraction operator
    
    //display stuff
    int a =1701;
    int b = 42;
    cout << "Let's perform an addition: " << a << "+" << b;
    cout << "="<< a+b << endl;
    //watch out: "a << b" is an affectation
    
    //to recover something:
    cout << "Give me a number: ";
    cin >> a;
    cout << "The number is " <<a<< endl;
    
    /* Console result:
        Hello world in C++
        Let's perform an addition: 1701+42=1743
        Give me a number: 9
        The number is 9
    */
    
    // special hint: you can do the same to read/write files !
    // but it's likely we won't have time to see that today
    // if you want more about this:
    // http://www.cplusplus.com/doc/tutorial/files/
    
    //array of char are hasbeen: introducting string type
    string str = "Hi! How are you ?";
    cout << str << endl;
    
    //can pass parameter by copy and pointer as before, but also
    //by reference !
    //more about references:
    //https://www.tutorialspoint.com/cplusplus/cpp_references.htm
    //(this website is great)
    
    //Same as before:
    cout << "Initial value: " << a << endl;
    increment_copy(a);
    cout << "After increment_copy: " << a << endl;
    increment_ptr(&a);
    cout << "After increment_ptr: " << a << endl;
    increment_ref(a);//allow to work without copy
    cout << "After increment_ref: " << a << endl;
    
    /* Console result:
        Initial value: 2
        After increment_copy: 2
        After increment_ptr: 3
        After increment_ref: 4
        
    This is the main use of ref, but other are possible
    */
    
    //more details: references can be use elsewhere:
    int c = 10;
    int& refC = c;
    cout << "c= " << c << " refC= "<< refC<< endl;
    cout << "Increasing refC:" << endl;
    refC++;
    cout << "c= " << c << " refC= "<< refC<< endl;
    
    /* Console result:
        c= 10 refC= 10
        Increasing refC:
        c= 11 refC= 11

        a reference is just a new label for something else

        references can be used in many ways, to learn more;
        https://www.tutorialspoint.com/cplusplus/cpp_references.htm
    */
    
    //constant: possible to define variable that's don't change:
    float const nb = 3.1416f;
    //this instruction can be used elsewhere
    // cout << "Initial value: " << a << endl;
    // increment_ref_c(a);//don't work, because of compiler
    // cout << "After increment_ref_c: " << a << endl;
    
    //dynamic allocation C++ style, same logic but different syntax
    int *d = new int;
    *d = 5;
    cout << *d; //display 5
    delete d; //free the memeory
    
    //for arrays:
    //non dynamical:
    //  int my_array[];
    //  int my_array[10];
    //dynamic:
    int *my_array = new int[10];
    
    //usefull object: vectors, see at:
    //https://www.tutorialspoint.com/cplusplus/cpp_stl_tutorial.htm
    
}
