#include "animal.hpp"

//constructor: automatically called to intitialize the object
Animal::Animal(std::string name){
    m_name = name;
    m_health_point=5;
    m_alive = true;
    
    //example if we have a attribut "age" (no m_ convention here)
    //this->age = age;
    //"this-> age" means the attribut of the object
    //age means the parameter given to the function
}

Animal::~Animal(){
    //useful only when an action need to be performed when
    //the object is destroyed, such as free the dynamic memory
    //if dynamic structures have been used
}

void Animal::display(){
    cout<<"Animal: "<< endl;
    cout << "\t Name: " << m_name <<endl;
    cout << "\t HP: " << m_health_point <<endl;
    cout << "\t Status: " << ((m_alive) ? "Alive" : "Dead") <<endl;
    
    //rq: ternary condition:
    // (condition) ? yes : no;
    //useful for affectation or steam
    //shorcut to
    // if(condition)
    //   yes
    // else
    //    no
}

void Animal::eat(int number, std::string food){
    cout << m_name << " eats " << number << " " << food << endl;
}

//when need to send object to function: use pointer or red, copies
//need time, if you want to be sure to not alter the object: 
void Animal::fight_with(Animal* p_animal){
    cout << m_name << " fights with " << p_animal->m_name << endl;
    this->take_dammage(1); //the one who attack loose 1HP
    p_animal->take_dammage(3); //the one who is attacked loose 2HP
    
    cout << "Status after fight:" <<endl;
    cout << "\t" << this->m_name <<" "<< m_health_point << " HP ["<< ((m_alive) ? "Alive" : "Dead") << "]" << endl;
    cout << "\t" << p_animal->m_name <<" "<< p_animal->m_health_point << " HP ["<< ((p_animal->m_alive) ? "Alive" : "Dead") << "]" << endl;
}

void Animal::take_dammage(int d){
    this->m_health_point -= d;
    if(this->m_health_point <= 0){
        this->m_alive = false;
        this->m_health_point = 0;
    }
}

int Animal::get_health_point() const{
    return m_health_point;
}

void Animal::set_health_point(int hp){
    m_health_point = hp;
}

void Animal::private_method(){
    //...
}

void do_smth(Animal p_an){
    
}
