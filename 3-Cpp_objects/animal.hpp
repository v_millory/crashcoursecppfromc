#include <iostream>
#include <string>

using namespace std;

/*
    create an object: like an evoluate structure
    class=blueprint for the instances of the object
    allow to perform encapsulation: have complexe system hidden
    in a kind of black box with well defined interfaces
*/
class Animal{
//a variable in a class is called attribute or a member
//new with respect to structures: methods=functions of the object

public: //can be accessed like structure, from outside the object
    std::string m_name;
    
    /*
        to access a variable of the object: just call its name as
        defined in the class. For instance, if in a methods we write:
        m_age = 2;
        =affects 2 to m_age of this instance of animal
        
        the m_ is just a personal convention to indicate it's member
        sometime, to avoid to bother/get confuse:
        use a classic name for the attribut: for instance age
        an use the pointer that indicate the object: "this" if
        need to access it from methods (from inside the object)
        (see constructor for example)
    */
    
    //public methods
    Animal(std::string name);//constructor
    ~Animal();  //destructor
    
    void display();
    void eat(int number, std::string food);
    void fight_with(Animal* p_animal);
    void take_dammage(int d);
    
    //getter and setter: access the private attributes
    int get_health_point() const;//const= don't change the objet
    void set_health_point(int hp);
    
private: //cannot be accessed from outside the object
    int m_health_point;
    bool m_alive; //status: alive or dead
    //int m_age; etc
    
    void private_method();
    
//other statut for attribut and methods: protected, we see it later

//rq: possible to change, to surcharge the actions of all operators
//      acting on an object ! For instance, possible to change <<
//      in order to write "cout << my_object"
};

//can define other function which don't belong to the class:
void do_smth(Animal p_an);
