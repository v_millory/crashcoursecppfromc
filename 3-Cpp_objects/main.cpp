#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

int main(int argc, char* argv[]){
    
    //create an animal
    Animal my_animal("Toto");
    my_animal.display(); //use a point to use public methods
    
    //create a dynamic animal (using pointer)
    Animal *my_2nd_animal = new Animal("Felix");
    my_2nd_animal->display();// replace the point by -> of pointer
    //reminder: prt->smth is a shortcut for (*ptr).smth
    
    my_2nd_animal->eat(2, "Mices");
    //display: Felix eats 2 Mices
    
    /* Consol result:
        Animal: 
        	 Name: Toto
        	 HP: 5
        	 Status: Alive
        Animal: 
        	 Name: Felix
        	 HP: 5
        	 Status: Alive
    */
    
    //access (public) variables:
    cout << my_animal.m_name<< endl;    //display Toto
    my_animal.m_name = "Toto II";//public= can be changed from outside
    //IT'S A BAD IDEA TO DO SO!!! Break the encapsulation      
    cout << my_animal.m_name << endl;    //display Toto II
    
    cout<<"Round 1:"<< endl;
    my_animal.fight_with(my_2nd_animal);
    cout<<"Round 2:"<< endl;
    my_animal.fight_with(my_2nd_animal);
    
    /* Consol result:
        Round 1:
        Toto II fights with Felix
        Status after fight:
        	Toto II 4 HP [Alive]
        	Felix 2 HP [Alive]
        Round 2:
        Toto II fights with Felix
        Status after fight:
        	Toto II 3 HP [Alive]
        	Felix 0 HP [Dead]
    */
    
    delete my_2nd_animal; //free memory
}
