#include "animal.hpp"

//constructor: automatically called to intitialize the object
Animal::Animal(std::string name, int age){
    m_name = name;
    m_age = age;
}

Animal::~Animal(){

}

void Animal::display(){
    cout<<"Animal: "<< endl;
    cout << "\t Name: " << m_name <<endl;
    cout << "\t Age: " << m_age <<endl;
}
