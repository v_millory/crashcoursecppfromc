#include <iostream>
#include <string>

using namespace std;


class Animal{
public: 
    std::string m_name;

    Animal(std::string name);
    ~Animal();
    void display();
    void play();
    
protected:
    int m_age;
};

//Dog inherit of the mother class Animal
class Dog: public Animal{
    
public:
    void fetch_the_stick();
};
