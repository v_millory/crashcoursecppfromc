#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

int main(int argc, char* argv[]){
    
    //create an animal
    Animal my_animal("Toto");
    my_animal.display();
    
    //create a dynamic animal
    Animal *my_2nd_animal = new Animal("Felix");
    my_2nd_animal->display();
    
    
    delete my_2nd_animal; //free memory
}

//https://www.tutorialspoint.com/cplusplus/cpp_inheritance.htm
